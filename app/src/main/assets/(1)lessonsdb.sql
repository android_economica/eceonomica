BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "lesson" (
	"_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"courseid"	INTEGER,
	"title"	TEXT,
	"section0"	TEXT,
	"section1"	TEXT,
	"section2"	TEXT,
	"section3"	TEXT,
	"section4"	TEXT,
	"section5"	TEXT,
	"section6"	TEXT,
	"section7"	TEXT,
	"section8"	TEXT,
	"section9"	REAL,
	"questions"	INTEGER,
	"result"	INTEGER DEFAULT 0,
	"nsections"	INTEGER DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "course" (
	"_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"title"	TEXT
);
INSERT INTO "lesson" VALUES (1,1,'Подтема 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (2,1,'Подтема 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (3,2,'Подтема 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (4,2,'Подтема 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (5,2,'Подтема 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (6,3,'Подтема 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (7,3,'Подтема 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (8,3,'Подтема 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (9,4,'Подтема 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (10,4,'Подтема 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (11,5,'Подтема 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (12,5,'Подтема 2',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (13,5,'Подтема 3',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (14,5,'Подтема 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (15,5,'Подтема 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (16,6,'Подтема 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (17,6,'Подтема 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (18,2,'Подтема 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (19,1,'Подтема 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (20,4,'Подтема 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (21,3,'Подтема 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (22,4,'Подтема 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (23,3,'Подтема 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (24,2,'Подтема 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (25,2,'Подтема 6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (26,5,'Подтема 6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (27,6,'Подтема 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (28,6,'Подтема 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "lesson" VALUES (29,6,'Подтема 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
INSERT INTO "course" VALUES (1,'Тема 1');
INSERT INTO "course" VALUES (2,'Тема 2');
INSERT INTO "course" VALUES (3,'Тема 3');
INSERT INTO "course" VALUES (4,'Тема 4');
INSERT INTO "course" VALUES (5,'Тема 5');
INSERT INTO "course" VALUES (6,'Тема 6');
COMMIT;
